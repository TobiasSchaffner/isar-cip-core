#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2019 - 2021
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require linux-cip-common.inc

KERNEL_DEFCONFIG_VERSION ?= "4.4.y-cip"

SRC_URI[sha256sum] = "0c59f3b3859c309a9a74d9709d70a35ea449868ebfa2fc56a0cc9d533f351000"
